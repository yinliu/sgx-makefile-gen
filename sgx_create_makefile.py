compiler = "/home/siddharth/llvm/clang-4.0.0.src/build/bin/clang"

sgx_path = "/home/siddharth/Downloads/sgxsdk"

obj_u = "OBJ_u ="
obj_t = "OBJ_t ="

cflags_u = "CFLAGS_u = -m64 -O0 -g -fPIC -Wno-attributes -IApp -I$(sgx_sdk_path)/include -DDEBUG -UNDEBUG -UEDEBUG"
cflags_t = "CFLAGS_t = -m64 -O0 -g -nostdinc -fvisibility=hidden -fpie -fstack-protector  -I$(sgx_sdk_path)/include -I$(sgx_sdk_path)/include/tlibc -I$(sgx_sdk_path)/include/stlport"

files_u = "Enclave_u.c, app_main.c, gen_sgx_utils.c, RPC_ClientStub.c"
files_t = "Enclave_t.c, Enclave.c, partitioned.c"

filelist_u = files_u.split(", ")
filelist_t = files_t.split(", ")

objectlist_u = []
objectlist_t = []


for i in range(len(filelist_u)):
	objectlist_u.append(filelist_u[i][:filelist_u[i].rfind(".")] + ".o")
	obj_u += " " + objectlist_u[i]

for i in range(len(filelist_t)):
	objectlist_t.append(filelist_t[i][:filelist_t[i].rfind(".")] + ".o")
	obj_t += " " + objectlist_t[i]

makefile_path = "Makefile"
makefile = open(makefile_path,"w")

makefile.write("CC = " + compiler + "\n\n")

makefile.write("sgx_sdk_path = " + sgx_path + "\n\n")

makefile.write(obj_u + "\n")
makefile.write(cflags_u + "\n\n")

makefile.write(obj_t + "\n")
makefile.write(cflags_t + "\n\n")

filesign_command = "\t$(sgx_sdk_path)/bin/x64/sgx_sign sign -key Enclave_private.pem -enclave enclave.so -out enclave.signed.so -config Enclave.config.xml\n"

makefile.write("sign: untrusted trusted\n")
makefile.write(filesign_command + "\n")

makefile.write("untrusted: proxy\n")

for i in range(len(filelist_u)):
	makefile.write("\t$(CC) $(CFLAGS_u) -c " + filelist_u[i] + " -o " + objectlist_u[i] + "\n")

executable_command_u = "\t$(CC) $(OBJ_u) -o app -m64 -O0 -g -L$(sgx_sdk_path)/lib64 -lsgx_urts -lpthread -lsgx_uae_service"
makefile.write(executable_command_u + "\n\n")

makefile.write("trusted: \n")

for i in range(len(filelist_t)):
	makefile.write("\t$(CC) $(CFLAGS_t) -c " + filelist_t[i] + " -o " + objectlist_t[i] + "\n")

enclave_so_command = "\t$(CC) $(OBJ_t) -o enclave.so -m64 -O0 -g -Wl,--no-undefined -nostdlib -nodefaultlibs -nostartfiles -L$(sgx_sdk_path)/lib64 -Wl,--whole-archive -lsgx_trts -Wl,--no-whole-archive -Wl,--start-group -lsgx_tstdc -lsgx_tstdcxx -lsgx_tcrypto -lsgx_tservice -Wl,--end-group -Wl,-Bstatic -Wl,-Bsymbolic -Wl,--no-undefined -Wl,-pie,-eenclave_entry -Wl,--export-dynamic -Wl,--defsym,__ImageBase=0"
makefile.write(enclave_so_command + "\n\n")

makefile.write("proxy:\n")
makefile.write("\t$(sgx_sdk_path)/bin/x64/sgx_edger8r --untrusted ./Enclave.edl --search-path . --search-path $(sgx_sdk_path)/include\n")
makefile.write("\t$(sgx_sdk_path)/bin/x64/sgx_edger8r --trusted ./Enclave.edl --search-path . --search-path $(sgx_sdk_path)/include\n\n")

makefile.write("clean:\n")
makefile.write("\trm *.o\n")
makefile.write("\trm app enclave.so enclave.signed.so\n")
makefile.write("\trm Enclave_t.*\n")
makefile.write("\trm Enclave_u.*")
